:- encoding(utf8).

s --> sn, sv.
sn --> det, n.
det --> [el]; [la].
n --> [abogado]; [libro].
sv --> v.
sv --> v, sn.
v --> [piensa]; [lee].
