:- encoding(utf8).

% Descomentar la gramática que se quiera cargar
%:- [work002]. % gram1
%:- [dcgsem]. % gram2
%:- [dcgparseoysem]. % gram3

% Según la gramática que se descomentó, correr alguno de los siguientes comandos:
gram1(1) :- tree_one([the,woman,sits,on,the,table]). % devuelve parseos según la cadena
gram1(2) :- tree_one([the,woman,sees,under,the,table]). % devuelve parseos según la cadena
gram1(3) :- tree_one([the,woman]). % devuelve parseos según la cadena
gram1(4) :- s(X,Y). % devuelve parseos y cadenas para s
gram1(5) :- vp(X,Y) % devuelve parseos y cadenas para vp
% se puede hacer lo mismo que en los dos de arriba con np y pp.
gram2(1) :- phrase(s(X),[juan,fuma]). % devuelve significado según la cadena
gram2(2) :- phrase(s(X),Y). % genera todas las posibilidades (actualmente solo juan fuma)
gram2(3) :- phrase(s(fumar(juan)),X). % genera en función del significado
gram3(1) :- phrase(s(X,Y),Z). % genera todas las posibilidades
% se puede especificar un significado en X y devuelve todos los parseos y cadenas
% se puede especificar un parseo en Y y devuelve todos los significados y cadenas
% se puede especificar una cadena en Z y devuelve todos los significados y parseos. 





