/*######################################################################*/
/*######################################################################*/
/*#   work002.pl                 March 27, 1997 		       #*/
/*#                                                                    #*/
/*#   V61.0024: Computational Principles of Sentence Construction      #*/
/*#  http://www.nyu.edu/pages/linguistics/courses/v610024	       #*/
/*#  http://www.nyu.edu/pages/linguistics/workbook/work002.pl          #*/
/*#                                                                    #*/
/*#   Check out this site at NYU Linguistics for information           #*/
/*#     about how to run this program                                  #*/
/*#                                                                    #*/
/*#   http://www.nyu.edu/pages/linguistics/workbook/2.2                #*/    
/*#                                                                    #*/
/*#   In SWI-PROLOG, this should be called:   work002.pl               #*/
/*#     and it should be in the directory c:\pl  or c:\pl\bin.         #*/
/*#                                                                    #*/
/*#  If you have any comments: doughert@acf2.nyu.edu                   #*/
/*######################################################################*/
/*######################################################################*/

/************************************************************************/
/************************************************************************/
/*********         Lexicon of Nouns, Determiners,          **************/
/*********     Prepositions and Verbs with no Lexical      **************/
/*********            Complement Restrictions              **************/
/************************************************************************/
/************************************************************************/

          /*   Program Name:  g0902     lexicon_09a    */
          /*   determiners                             */
          det([the]).
          det([a]).
          det([this]).
          det([that]).
          /*   prepositions                            */
          prep([on]).
          prep([in]).
          prep([at]).
          prep([under]).
          prep([to]).
          /*   nouns                                   */
          noun([house]).
          noun([table]).
          noun([man]).
          noun([woman]).
	  noun([hippopotamus]).
	  noun([subcontinent]).
          /*   verbs                                   */
          verb([sees]).
          verb([sits]).
          verb([looks]).
          verb([persuades]).
          verb([thinks]).
          verb([knows]).
          verb([puts]).
	  verb([lives]).


/************************************************************************/
/************************************************************************/
/*********         Principles of Combination for Noun      **************/
/*********     Phrases, Verb Phrases, and Prepositional    **************/
/*********             Phrases and Sentences               **************/
/************************************************************************/
/************************************************************************/

          /*   Program Name:  g0903                    */   
          detvar(0).               
          detail         :-        retract(detvar(_)),assert(detvar(1)).   
          no_detail      :-        retract(detvar(_)),assert(detvar(0)).   
          s(X,LBS)       :-             append(A,B,X),np(A,LBNP),vp(B,LBVP),
                         /*   construct labeled bracketing for S  */  
                                        append(['(s_'],LBNP,Z1),           
                                        append(Z1,LBVP,Z2),           
                                        append(Z2,[')'],LBS),              
                                        (detvar(0);write(LBS),nl).    
          vp(X,LBVP)     :-             append(A,B,X),verb(A),pp(B,LBPP),  
                         /*   construct labeled bracketing  */   
                                        append(['(vp_(v_'],A,Z1),          
                                        append(Z1,[')'],Z2),               
                                        append(Z2,LBPP,Z5),           
                                        append(Z5,[')'],LBVP),        
                                        (detvar(0);write(LBVP),nl).        
          pp(X,LBPP)          :-        append(A,B,X),prep(A),np(B,LBNP),  
                         /*   construct labeled bracketing   */  
                                        append(['(pp_(p_'],A,Z1),          
                                        append(Z1,[')'],Z2),               
                                        append(Z2,LBNP,Z5),                
                                        append(Z5,[')'],LBPP),             
                                        (detvar(0);write(LBPP),nl).        
          np(X,LBNP)     :-             append(A,B,X),det(A),noun(B), 
                         /*   construct labeled bracketing  */   
                                        append(['(np_(det_'],A,Z1),        
                                        append(Z1,[')(n_'],Z2),            
                                        append(Z2,B,Z3),              
                                        append(Z3,['))'],LBNP),nl,         
                                        (detvar(0);write(LBNP),nl). 
       
/************************************************************************/
/************************************************************************/
/*********    tree_one(LIST) displays trees on screen      **************/
/*********    tree_prn(LIST) writes trees to file          **************/
/*********      each parse tree is in a different file     **************/
/*********          wbfile1, wbfile2, wbfile3...           **************/
/*********    the files wbfileN can be edited as ASCII     **************/
/************************************************************************/
/************************************************************************/


tree_one(List):-		       /* display parse trees on screen */
        (np(List,Labeled_List),
        trans(Labeled_List,Chris_List),
        name(Chris_Atom,Chris_List),
        tell(fred),
        write(Chris_Atom),
        write('.'),
	nl, nl,
        told,
        see(fred),
        read(Structure),
        seen,
        drucke_baum(Structure)),fail.

tree_prn(List) :- s(List,Labeled_List),    /* writes parse trees to files */
		change(Labeled_List,Structure),gensym(wbfile,FILE),
		nl,write('Writing phrase marker to FILE: '),write(FILE),
		nl,write('tree_prn('),write(List),write(')'),nl,
		tell(FILE),
		write('FILENAME: '),write(FILE),
		nl,write('tree_prn('),write(List),write(')'),nl,nl,
		write('Beginners Workbook in Computational Linguistics'), 
		nl,write('PROGRAM FILE: work002 '),
		nl,write('Prolog Function Name: tree_prn(LIST)'),
		nl,write('LIST = '),write(List),		
		nl,nl,
		drucke_baum(Structure),told.

tree_1(List) :- s(List,Labeled_List),
		change(Labeled_List,Structure),
		drucke_baum(Structure).

change(Labeled_List,Structure) :-
	trans(Labeled_List,Chris_List),
        name(Chris_Atom,Chris_List),
        tell(fred),
        write(Chris_Atom),
        write('.'),
	nl, nl,
        told,
        see(fred),
        read(Structure),
        seen.

/************************************************************************/
/************************************************************************/
/*********          Readjustment Program to convert        **************/
/*********       (s_(np_(det_the)(n_girl))(vp_(v_sees)...  **************/
/*********                    to                           **************/
/*********       s(np(det(the),n(girl)),(vp(v(sees)...     **************/
/************************************************************************/
/************************************************************************/


member(X,[X|_]).
member(X,[_|R]):-
        member(X,R).

append([],L,L).
append([K|R],L,[K|R1]):-
        append(R,L,R1).
not(X):-
        \+(X).

tree_one(List):-
        s(List,Labeled_List),
        trans(Labeled_List,Chris_List),
        name(Chris_Atom,Chris_List),
        tell(fred),
        write(Chris_Atom),
        write('.'),
	nl, nl,
        told,
        see(fred),
        read(Structure),
        seen,
        drucke_baum(Structure).

tree_two(List):-
        s(List,Labeled_List),
        trans(Labeled_List,Chris_List),
        name(Chris_Atom,Chris_List),
        tell(file1),
        write(Chris_Atom),
        write('.'),
	nl, nl,
        told,
        see(file1),
        read(Structure),
        seen,
	tell('file2.txt'),
        drucke_baum(Structure),
	told.




name_all([],[]):- !.
name_all([Z|R],[Z1|R1]):-
        name(Z,Z1),
        name_all(R,R1).

append_all([],[]):-!.
append_all([X|R],L):-
        append_all(R,L1),
        append(X,L1,L).


subst(_,_,[],[]):- !.
subst(X,Y,[Z|R],[Y|R1]):-
        not(X \== Z),
        !,
        subst(X,Y,R,R1).
subst(X,Y,[Z|R],[Z|R1]):-
        subst(X,Y,R,R1).

del_all(_,[],[]):- !.
del_all(X,[Z|R],R1):-
        X == Z,
        !,
        del_all(X,R,R1).
del_all(X,[Z|R],[Z|R1]):-
        del_all(X,R,R1).


ins(_,_,[],[]):- !.
ins([X,Y],[Z],[X,Y|R],[X,Z,Y|R1]):-
        !,
        ins([X,Y],[Z],R,R1).
ins([X,Y],[Z],[U|R],[U|R1]):-
        ins([X,Y],[Z],R,R1).

trans(S,S1):-
                                name_all(S,L),
                                append_all(L,L1),
        [X] = "(",
        [Y] = "_",
        ins(")(",",",L1,S2),
        del_all(X,S2,S3),
        subst(Y,X,S3,S1).


/************************************************************************/
/************************************************************************/
/*********     Christoph Lehner's Tree Drawing Program     **************/
/*********                  SWI Prolog                     **************/
/*********  input:   s(np(det(the),n(girl)),(vp(v(sees)... **************/
/*********  output:  an ASCII phrase marker                **************/
/************************************************************************/
/************************************************************************/

/* Program is taken from my Prolog text book
Christoph Lehner: Prolog und Linguistik. Oldenbourg, M|nchen 1992, 2. Auflage.
Out of print, now */

ana(Baum,[k(Baum,Pos)],L_aussen,R_aussen):-
                      atomic(Baum),
                      !,
                      atomic_laenge(Baum,N),           
                      R_aussen is L_aussen + N + 2,
                      Pos is (R_aussen + L_aussen ) // 2 .

ana(Baum,[k(F,Pos),[k(Nachfolger,Pos)]],L_aussen,R_aussen):-
            Baum =.. [F,Nachfolger],
            atomic(Nachfolger),
            !,
            atomic_laenge(Nachfolger,N1),
            atomic_laenge(F,N2),
            max(N1,N2,N),
            R_aussen is L_aussen + N + 2, 
            Pos is (R_aussen + L_aussen) // 2.

ana(Baum,[k(F,Pos),L],L_aussen,R_aussen):-
            Baum =.. [F|Nachfolger],
            atomic_laenge(F,N),
            M is L_aussen + N + 2,
            ana_nachfolger(Nachfolger,L_aussen,R,L),
            berechne_pos(L_aussen,Pos,L,R,M,R_aussen).           

berechne_pos(Links,Pos,L,R,M,R):- M =< R,
                            !,
                            erster_knoten(L,Pos1),
                            letzter_knoten(L,Pos2),
                            Pos is (Pos1 + Pos2) // 2.


berechne_pos(Links,Pos,L,R,M,M):- M > R,
                            Pos is (M + Links) // 2.



ana_nachfolger([Baum|Rest],L_aussen,R_aussen,L):-
             Rest \= [],
             ana(Baum,L1,L_aussen,Mitte),
             ana_nachfolger(Rest,Mitte,R_aussen,L2),
             append(L1,L2,L).

ana_nachfolger([Baum],L_aussen,R_aussen,L):-
           ana(Baum,L,L_aussen,R_aussen).

               
max(X,Y,X):- X >= Y, !.
max(X,Y,Y).




drucke_baum(S):- 
		sv(S),
		ana(S,L,0,R), 
                dr_baum(L).



dr_baum(L):-
          L = [_|_],
          drucke_knoten(L,0),
          nl,
          drucke_aeste(L,0),
          nl,
          drucke_zweige(L,L1,0),
          nl,
          dr_baum(L1).

dr_baum([]).




drucke_knoten([X,Y|R],Spalte1):-
                 Y \= [_|_],
                 !,
                 dr_kn(X,Spalte1,Spalte2),
                 drucke_knoten([Y|R],Spalte2).


drucke_knoten([X,[_|_]|R],S1):-
              dr_kn(X,S1,S2),
              drucke_knoten(R,S2).

drucke_knoten([X],S):-
                dr_kn(X,S,_).

drucke_knoten([],_).

dr_kn(k(X,Pos),S1,S2):-
             atomic_laenge(X,N),
             tab(Pos - N//2 - S1),
             S2 is Pos + N//2 + N mod 2,
             write(X).


/* Blaetter */
drucke_aeste([X,Y|Rest],S):-
                Y \= [_|_],
                !,
                drucke_aeste([Y|Rest],S).

drucke_aeste([X],S):-
                !.

/* nicht-verzweigende, z.B. lexikalische Kategorien */
drucke_aeste([k(X,Pos),L|Rest],S1):-
                knoten_zahl(L,1),
                !,
                tab(Pos-S1),
                write(|),
                S2 is Pos + 1 ,
                drucke_aeste(Rest,S2).

/* normale Kategorien */
drucke_aeste([X,[K|R]|Rest],S1):-
                !, 
                dr_ae(X,[K|R],S1,S2),
                drucke_aeste(Rest,S2).

drucke_aeste([],_).


/* dr_ae(Dominierender_Knoten,[Tochter_links,L1,Tochter_rechts,L2]  */
 
                    
dr_ae(k(X,Pos),L,S1,S2) :-
                        erster_knoten(L,Pos1),
                        letzter_knoten(L,Pos2),
                        Mitte is (Pos1 + Pos2) // 2,
                        tab(Pos1-S1+1),
                        n_mal(Mitte-Pos1-1,'_'),
                        write(|),
                        n_mal(Pos2-Mitte-2,'_'),
                        S2 is Pos2 - 1.

erster_knoten([k(_,Pos)|L],Pos).


/* einen letzten Knoten gibt es nur dann, wenn
   es einen ersten Knoten gibt */
letzter_knoten([_|R],Pos):-
                         letzter_knoten(R,Pos).

/* 1. Fall: Blatt */
letzter_knoten([k(_,Pos)],Pos):- !.
/* 2. Fall: dominierender Knoten */
letzter_knoten([k(_,Pos),[_|_]],Pos).



/************************************************/
/* Blaetter */
drucke_zweige([X,Y|Rest],Rest1,S1):-
                Y \= [_|_],
                !,
                drucke_zweige([Y|Rest],Rest1,S1).

drucke_zweige([X],[],S):-
                X \= [_|_],
                !.


/* nicht-verzweigende, z.B. lexikalische Kategorien */
drucke_zweige([k(_,Pos),X|Rest],L,S1):-
                knoten_zahl(X,1),
                !,
                tab(Pos-S1),
                write(|),
                S2 is Pos + 1,
                drucke_zweige(Rest,L1,S2),
                append(X,L1,L).

/* normale Kategorien */
drucke_zweige([X,[K|R]|Rest],L,S1):-
                !,
                dr_zw(X,[K|R],S1,S2),
                drucke_zweige(Rest,L1,S2),
                append([K|R],L1,L).


drucke_zweige([],[],_).


/* dr_zw(Dominierender_Knoten,[Tochter_links,L1,Tochter_rechts,L2]  */
                     
dr_zw(k(X,Pos),L,S1,S4) :-
                       erster_knoten(L,Pos1),
                       tab(Pos1-S1),
                       write(/),
                       S2 is Pos1+1,
                       knoten_dazwischen(L,S2,S3),
                       letzter_knoten(L,Pos2),
                       tab(Pos2 - S3 - 1),
                       write('\'),
                       S4 is Pos2 .


knoten_dazwischen([k(_,_)|R],S1,S2):-
                    drucke_zweige_fuer_knoten(R,S1,S2).


drucke_zweige_fuer_knoten([k(_,_)],S,S):- !.
drucke_zweige_fuer_knoten([k(_,_),[Letzte|Nachfolger]],S,S):- !.
drucke_zweige_fuer_knoten([k(_,Pos)|R],S1,S2):-
                           !,
                           tab(Pos-S1),
                           write(|),
                           S3 is Pos + 1,
                           drucke_zweige_fuer_knoten(R,S3,S2).
drucke_zweige_fuer_knoten([_|R],S1,S2):-
                           drucke_zweige_fuer_knoten(R,S1,S2).


knoten_zahl([k(_,_)|L],1):- not member_x(k(_,_),L). 




n_mal(Arith,A):- X is Arith,
                 n_mal_x(X,A).

n_mal_x(N,A):- N > 0,
               !,
               write(A),
               M is N - 1,
               n_mal(M,A).
n_mal_x(N,_):- N =< 0.



atomic_laenge(A,N):- atom(A),
                     !, 
                     name(A,L),
                     listen_laenge(L,N).

atomic_laenge(Nr,N):- numeric(Nr),
                     !, 
                     name(Nr,L),
                     listen_laenge(L,N).

atomic_laenge(X,N):- var(X),
                     !. 

listen_laenge([_|R],N):- listen_laenge(R,M),
                         !,
                         N is M + 1.
listen_laenge([],0). 
 

member_x(X,L):- member(X,L), !.



numeric(X):- number(X).


/*    instanziieren noch freier     */
 /*    variablen                     */

 sv(X):- sv(X,1,_).
 
 
 sv(X, N, N1):-
    var(X),
    !,
 
    nextvar(X,N),
    N1 is N+1 .
 sv([X | Y], N, N2):-
    !,
    sv(X, N, N1),
    sv(Y, N1, N2).
 
 sv(S,N,N1):-
    compound(S),
    S =..[F|A],
    !,
    sv(A,N,N1).

 sv(X, N, N).


compound(X):-
	nonvar(X),
	not(atomic(X)).


 
nextvar('X',1).
nextvar('Y',2).
nextvar('Z',3).
nextvar('U',4).
nextvar('V',5).
nextvar('W',6).
nextvar(X,N):- N > 6,
               number(N,L),
               append("X",L,Y),
               name(X,Y).

atomic_length(X,5):- var(X), !.
atomic_length(X,N):- 
                  name(X,L),
                  list_length(L,N).
list_length([],0).
list_length([K|R],N):- list_length(R,M),
                       N is M + 1.

term_laenge(S,N):- atomic(S),
                   !,
                   atomic_length(S,N).
term_laenge(S,N):- S \= [_|_],
                   !,
                   S =..[F|A],
                   list_length(A,L),
                   atomic_length(F,X),
                   alle_args(A,Y),
                   N is X + Y + 2 + L - 1.

term_laenge(L,N):- L = [_|_],
                   alle_args(L,X),
                   list_length(L,Y),
                   N is X + 2 + Y - 1.
alle_args([],0).

alle_args([K|R],N):- 
              term_laenge(K,X),
              alle_args(R,Y),
              N is X + Y.
              
/* Test-Trees */


:- drucke_baum(s(np(det(the),noun(man)),vp(v(sees)))).
:- drucke_baum(s(np(det(the),noun(man)),vp(v(sees),pp(in_the_house)))).
:- drucke_baum(s(np(det(the),noun(man)),vp(v(sees),pp(p(in),np(the_house))))).
:- drucke_baum(s(np(det(the),noun(man)),vp(v(sees),pp(p(in),np(det(the),noun(house)))))).



/************************************************************/
/************************************************************/
/************************************************************/

          /*   Program Name:  g0904 */
          function1(X,Y)      :-   function1a(X,A),name(Y,A).
          function1a([],[]).
          function1a([X|Y],Z)      :-   name(X,A),append(A,Z1,Z),
                                        function1a(Y,Z1).
          function2(X,Y)           :-   function2a(X,A),name(Y,A).
          function2a([],[]).
          function2a([X|Y],Z)      :-   name(X,A),append(A,[32],B),
                                        append(B,Z1,Z),function2a(Y,Z1).
          function3("").
          function3(X)             :-   name(X,A),output(A).
          output([]).
          output([X|Y])            :-   (X==95,write(' '),output(Y));
                                        (name(Z,[X]),write(Z),output(Y)).
	  
          /*   Program Name:  g0906                    */   
          tree(X)                :-      s(X,A),function1(A,B),out(B).   
          labelbracket(X)        :-      s(X,A),function1(A,B),write(B).   
          out(X)    :-   assert(lm(0)),name(X,A),do(A),retract(lm(_)).  
          do([]).                                      
          do([X|Y])      :-        ((X==40),uplm,nl,tabover,do(Y));        
                                   ((X==41),dnlm,tabover,do(Y));        
                                   (name(A,[X]),write(A),do(Y)).      
          uplm           :-   lm(A),B is A+5,retract(lm(_)),assert(lm(B)). 
          dnlm           :-   lm(A),B is A-5,retract(lm(_)),assert(lm(B)). 
          tabover        :-   lm(X),writesp(X).                  
          writesp(X)     :-        (                        
                                   (X==0);                       
                               	   (write(' '),Y is X-1,writesp(Y))       
                                   ).                      

