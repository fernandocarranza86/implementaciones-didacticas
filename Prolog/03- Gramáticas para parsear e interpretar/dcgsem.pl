%Autor: Fernando Carranza (adaptado de la propuesta por Tim Smith en su guía de clase "Parsing and Semantics in DCG" del curso "Artificial Intelligence Programming in Prolog" del 2004, disponible en "http://www.inf.ed.ac.uk/teaching/courses/aipp/lecture_slides/11_PS_DCGs.pdf")
% Instrucciones: Para generar oraciones llamar en la terminal desde Prolog y preguntar alguna de las siguientes dos opciones: a) s(X,Y,[]). b) phrase(s(X),Y). Para chequear si determinada oración o determinado significado es gramatical, reemplazar las variables por constantes. Si se llama de esa forma, se dan las siguientes equivalencias: X=significado, Y=cadenas generadas.

:- encoding(utf8).

s(Pred) --> n(Actor), v(Actor, Pred).
n(juan) --> [juan].
v(Actor, fumar(Actor)) --> [fuma].
