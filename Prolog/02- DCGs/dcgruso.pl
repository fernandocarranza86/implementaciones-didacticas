:- encoding(utf8),

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ejercicio DCG
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Elabore una mini DCG del ruso que genere/reconozca las oraciones marcadas como gramaticales y 
% rechace las marcadas como agramaticales. Entre paréntesis hemos agregado una glosa orientativa
% Esa glosa no forma parte de la cobertura de la gramática.
% 
% Ja piu piba (yo tomo cerveza)
% Ja piu badu (yo tomo agua)
% Ty piosh piba (vos tomás cerveza)
% Ty piosh badu (vos tomás agua)
% On piot piba (él toma cerveza)
% On piot badu (él toma agua)
% Ona piot piba (ella toma cerveza)
% Ona piot badu (ella toma agua)
% Ja pil piba (yo tomé-masc cerveza)
% Ja pila piba (yo tomé-fem cerveza)
% Ja pil badu (yo tomé-masc agua)
% Ja pila badu (yo tomé-fem cerveza)
% Ty pil piba (vos tomaste-masc cerveza)
% Ty pila piba (vos tomaste-fem cerveza)
% Ty pil badu (vos tomaste-masc agua)
% Ty pila badu (vos tomaste-fem agua)
% On pil piba (él tomó-masc cerveza)
% Ona pila piba (ella tomó-fem cerveza)
% On pil badu (él tomó-masc agua)
% Ona pila badu (ella tomó-fem agua)
% *On pila piba (él tomó-fem cerveza)
% *Ona pil piba (ella tomó-masc cerveza)
% *On pila badu (él tomó-fem agua)
% *Ona pil badu (ella tomó-masc agua)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



