:- encoding(utf8),

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ejercicio DCG
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Elabore una mini DCG para un fragmento del español que genere/reconozca las oraciones marcadas 
% como gramaticales y rechace las marcadas como agramaticales. 
% 
% Cobertura positiva:
% María miró la pared cansada.
% María miró el piso cansada.
% Pablo miró la pared cansado
% Pablo miró el piso cansado.
% María miró la pared.
% María miró el piso.
% Pablo miró la pared.
% Pablo miró el piso.
% María estuvo cansada.
% Pablo estuvo cansado.
% María miró la pared cansada ayer.
% María miró el piso cansada ayer.
% Pablo miró la pared cansado ayer.
% Pablo miró el piso cansado ayer.
% María miró la pared ayer.
% María miró el piso ayer.
% Pablo miró la pared ayer.
% Pablo miró el piso ayer.
% María estuvo cansada ayer.
% Pablo estuvo cansado ayer.
% María mirará la pared mañana
% Pablo mirará la pared mañana.
% María mirará el piso mañana.
% Pablo estará cansado mañana.
% María estará cansada mañana.
% Cobertura negativa:
% *María miró la pared cansado.
% *María miró el piso cansado.
% *Pablo miró la pared cansada.
% *Pablo miró el piso cansada.
% *María miró.
% *Pablo miró.
% *María estuvo cansado.
% *Pablo estuvo cansada.
% *María estuvo.
% *Pablo estuvo.
% *María miró la pared cansada mañana.
% *María miró el piso cansada mañana.
% *Pablo miró la pared cansado mañana.
% *Pablo miró el piso cansado mañana.
% *María miró la pared mañana.
% *María miró el piso mañana.
% *Pablo miró la pared mañana.
% *Pablo miró el piso mañana.
% *María estuvo cansada mañana .
% *Pablo estuvo cansado mañana.
% *María mirará la pared ayer.
% *Pablo mirará el piso ayer.
% *Pablo estará cansado ayer.
% *María estará cansada ayer.
% *María mirará.
% *Pablo mirará.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



