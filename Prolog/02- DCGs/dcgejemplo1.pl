:- encoding(utf8).

s --> sn(_Gen, Num), sv(Num).
sn(Gen, Num) --> det(Gen, Num), n(Gen, Num).
det(m, sg) --> [el].
det(f, sg) --> [la].
det(m, pl) --> [los].
det(f, pl) --> [las].
n(m, sg) --> [lingüista]; [artículo].
n(m, pl) --> [lingüistas]; [artículos].
n(f, sg) --> [lingüista]; [revista].
n(f, pl) --> [lingüistas]; [revistas].
sv(Num) --> v(Num).
sv(Num) --> v(Num), sn(_Gen, _Num).
v(sg) --> [piensa]; [lee].
v(pl) --> [piensan]; [leen].
