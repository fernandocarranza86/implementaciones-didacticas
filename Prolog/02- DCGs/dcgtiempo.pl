:- encoding(utf8),

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ejercicio DCG
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Elabore una mini DCG para un fragmento del español que genere/reconozca las oraciones marcadas 
% como gramaticales y rechace las marcadas como agramaticales. 
% 
% Cobertura positiva:
% a) El deportista corrió ayer.
% b) Los deportistas corrieron ayer.
% c) La deportista corrió ayer.
% d) Las deportistas corrieron ayer.
% e) El deportista correrá mañana.
% f) Los deportistas correrán mañana.
% g) La deportista correrá mañana.
% h) Las deportistas correrán mañana.
% Cobertura negativa:
% i) *El deportista corrió mañana.
% j) *El deportista correrá ayer.
% k) *El deportistas corrió ayer.
% l) *Los deportista correrá mañana.
% m) *Los deportistas correrá mañana.
% n) *La deportista corrieron ayer.
% ñ) *Las deportistas corrió ayer.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



